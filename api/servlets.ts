export class Cookie {
  private deleted: boolean;

  constructor(
    readonly name: string,
    readonly value: string,
  ) {
  }

  /**
   * Marks this cookie as to be deleted.
   */
  delete(): void {
    this.deleted = true;
  }

  get isDeleted(): boolean {
    return this.deleted;
  }
}

export interface HttpServletRequest {

  /**
   * Returns an array containing all of the Cookie objects the client sent with this request.
   */
  getCookies(): Cookie[];

  /**
   * Returns the header value for the given name.
   * @param name the name of the header value to return.
   */
  getHeader(name: string): string;

  /**
   * Return the HTTP method that was used for this request.
   */
  getMethod(): string;

  /**
   * Returns the query string that is contained in the request URL after the path. This method returns null if the URL does not have a query string.
   */
  getQueryString(): string;
}

export interface HttpServletResponse {
  addCookie(cookie: Cookie): void;
}
